export const typeOptions = [
  { title: "Film", value: "Film" },
  { title: "Short Film", value: "Short Film" },
  { title: "Documentary", value: "Documentary" },
  { title: "TV Series", value: "TV Series" },
 
];

export const statusOptions = [
  { title: "Public", value: "public" },
  { title: "Private", value: "private" },
];

export const languageOptions = [
  { title: "English", value: "English" },
  { title: "Dzongkha", value: "Dzongkha" },
  { title: "Tshangla", value: "Tshangla" },
  { title: "Lhotshampa", value: "Lhotshampa" },
  { title: "Khangkha", value: "Khangkha" },
  
];
